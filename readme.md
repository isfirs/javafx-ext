# JavaFx-ext
> A small suite of classes to help build JavaFX applications

[ ![Download](https://api.bintray.com/packages/isfirs/maven/javafx-ext/images/download.svg) ](https://bintray.com/isfirs/maven/javafx-ext/_latestVersion)

## Usage

Add the bintray repository to your existing ones:
```groovy
repositories {
    maven {
        url 'https://dl.bintray.com/isfirs/maven'
    }
}
```

Add the dependency
```groovy
dependencies {
    implementation 'isfirs:javafx-ext:0.1.0'
}
```
