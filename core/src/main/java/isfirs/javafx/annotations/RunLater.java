package isfirs.javafx.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicator, this method runs as a means of {@link javafx.application.Platform#runLater(Runnable)}.
 *
 * @author Isfirs
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RunLater {
}
