package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
public interface OldValueInvoker<T> extends Unbinder {

    void invoke(T oldValue);

}
