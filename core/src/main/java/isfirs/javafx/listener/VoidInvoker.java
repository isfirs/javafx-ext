package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
public interface VoidInvoker extends Unbinder {

    void invoke();

}
