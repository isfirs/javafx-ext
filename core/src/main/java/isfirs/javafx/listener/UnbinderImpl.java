package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
abstract class UnbinderImpl implements Unbinder {

    private final Runnable disposer;

    protected UnbinderImpl(final Runnable disposer) {
        this.disposer = disposer;
    }

    @Override
    public void dispose() {
        disposer.run();
    }

}
