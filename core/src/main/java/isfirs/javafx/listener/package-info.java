/**
 * Easy access utility for property listeners.
 *
 * @author Isfirs
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.listener;

import javax.annotation.ParametersAreNonnullByDefault;