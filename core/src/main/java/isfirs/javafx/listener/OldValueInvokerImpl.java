package isfirs.javafx.listener;

import java.util.function.Consumer;

/**
 * @author Isfirs
 */
final class OldValueInvokerImpl<T> extends UnbinderImpl implements OldValueInvoker<T> {

    private final Consumer<T> callback;

    protected OldValueInvokerImpl(final Runnable disposer, final Consumer<T> callback) {
        super(disposer);
        this.callback = callback;
    }

    @Override
    public void invoke(final T oldValue) {
        callback.accept(oldValue);
    }

}
