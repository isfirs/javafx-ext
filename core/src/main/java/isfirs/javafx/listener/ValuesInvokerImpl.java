package isfirs.javafx.listener;

import java.util.function.BiConsumer;

/**
 * @author Isfirs
 */
final class ValuesInvokerImpl<T> extends UnbinderImpl implements ValuesInvoker<T> {

    private final BiConsumer<T, T> callback;

    ValuesInvokerImpl(final Runnable disposer, final BiConsumer<T, T> callback) {
        super(disposer);
        this.callback = callback;
    }

    @Override
    public void invoke(final T oldValue, final T newValue) {
        callback.accept(oldValue, newValue);
    }
}
