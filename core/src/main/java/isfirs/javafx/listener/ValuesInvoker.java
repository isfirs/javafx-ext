package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
public interface ValuesInvoker<T> extends Unbinder {

    void invoke(T oldValue, T newValue);

}
