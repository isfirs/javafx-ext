package isfirs.javafx.listener;

import isfirs.javafx.util.Wip;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import javax.annotation.Nullable;
import java.util.function.Consumer;

/**
 * @author Isfirs
 */
@Wip
public final class ListListener {

    public static <T> Unbinder onNewValue(final ObservableList<T> list, final Consumer<T> callback) {
        return install(list, callback, null, null, null);
    }

    public static <T> Unbinder forRemovedValue(final ObservableList<T> list, final Consumer<T> callback) {
        return install(list, callback, null, null, null);
    }

    public static <T> Unbinder onAddOrRemove(final ObservableList<T> list, final Consumer<T> addCallback,
                                             final Consumer<T> removeCallback) {
        return install(list, addCallback, removeCallback, null, null);
    }

    public static <T> Unbinder onUpdate(final ObservableList<T> list, final Consumer<T> callback) {
        return install(list, null, null, callback, null);
    }

    public static <T> Unbinder forPermutation(final ObservableList<T> list, final Runnable callback) {
        return install(list, null, null, null, callback);
    }

    private static <T> Unbinder install(final ObservableList<T> list,
                                        @Nullable final Consumer<T> addCallback,
                                        @Nullable final Consumer<T> removeCallback,
                                        @Nullable final Consumer<T> updateCallback,
                                        @Nullable final Runnable permutateCallback) {
        ListChangeListener<T> lcl = c -> {
            while (c.next()) {
                if (c.wasAdded()) {
                    if (addCallback == null) {
                        return;
                    }
                    c.getAddedSubList().forEach(addCallback);
                } else if (c.wasRemoved()) {
                    if (removeCallback == null) {
                        return;
                    }
                    c.getRemoved().forEach(removeCallback);
                } else if (c.wasUpdated()) {
                    if (updateCallback == null) {
                        return;
                    }
                    for (int i = c.getFrom(); i < c.getTo(); i++) {
                        updateCallback.accept(c.getList().get(i));
                    }
                } else if (c.wasPermutated()) {
                    if (permutateCallback == null) {
                        return;
                    }
                    permutateCallback.run();
                }
            }
        };
        list.addListener(lcl);
        return () -> list.removeListener(lcl);
    }

    private ListListener() {
        throw new UnsupportedOperationException("Can not instantiate utility class");
    }
}
