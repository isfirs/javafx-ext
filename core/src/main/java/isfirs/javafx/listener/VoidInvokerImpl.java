package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
final class VoidInvokerImpl extends UnbinderImpl implements VoidInvoker {

    private final Runnable callback;

    VoidInvokerImpl(final Runnable disposer, final Runnable callback) {
        super(disposer);
        this.callback = callback;
    }

    @Override
    public void invoke() {
        callback.run();
    }

}
