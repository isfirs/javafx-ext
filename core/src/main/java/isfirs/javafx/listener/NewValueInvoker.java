package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
public interface NewValueInvoker<T> extends Unbinder {

    void invoke(T newValue);

}
