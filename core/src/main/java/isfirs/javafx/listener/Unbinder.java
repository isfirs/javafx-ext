package isfirs.javafx.listener;

/**
 * @author Isfirs
 */
@FunctionalInterface
public interface Unbinder {

    void dispose();

}
