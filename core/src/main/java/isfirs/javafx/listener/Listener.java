package isfirs.javafx.listener;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import org.jetbrains.annotations.Contract;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Utility class to install various callbacks on properties.
 *
 * @author Isfirs
 */
public final class Listener {

    public static <T> VoidInvoker onChange(final ObservableValue<T> observable, final Runnable callback) {
        return onChange(observable, callback, false);
    }

    public static <T> VoidInvoker onChange(final ObservableValue<T> observable, final Runnable callback, final boolean invokeOnce) {
        final Runnable disposer = install(observable, (p, o, n) -> callback.run());
        final VoidInvoker invoker = new VoidInvokerImpl(disposer, callback);
        if (invokeOnce)
            invoker.invoke();

        return invoker;
    }

    public static <T> NewValueInvoker<T> onNewValue(final ObservableValue<T> observable, final Consumer<T> callback) {
        return onNewValue(observable, callback, false);
    }

    public static <T> NewValueInvoker<T> onNewValue(final ObservableValue<T> observable, final Consumer<T> callback, final boolean invokeOnce) {
        final Runnable disposer = install(observable, (p, o, n) -> callback.accept(n));
        final NewValueInvoker<T> invoker = new NewValueInvokerImpl<>(disposer, callback);
        if (invokeOnce)
            invoker.invoke(observable.getValue());

        return invoker;
    }

    public static <T> OldValueInvoker<T> onOldValue(final ObservableValue<T> observable, final Consumer<T> callback) {
        return onOldValue(observable, callback, false);
    }

    public static <T> OldValueInvoker<T> onOldValue(final ObservableValue<T> observable, final Consumer<T> callback, final boolean invokeOnce) {
        final Runnable disposer = install(observable, (p, o, n) -> callback.accept(o));
        final OldValueInvoker<T> invoker = new OldValueInvokerImpl<>(disposer, callback);
        if (invokeOnce)
            // TODO NULL or getValue()?
            invoker.invoke(observable.getValue());

        return invoker;
    }

    public static <T> ValuesInvoker<T> onValues(final ObservableValue<T> observable, final BiConsumer<T, T> callback) {
        return onValues(observable, callback, false);
    }

    public static <T> ValuesInvoker<T> onValues(final ObservableValue<T> observable, final BiConsumer<T, T> callback, final boolean invokeOnce) {
        final Runnable disposer = install(observable, (p, o, n) -> callback.accept(o, n));
        final ValuesInvoker<T> invoker = new ValuesInvokerImpl<>(disposer, callback);
        if (invokeOnce)
            invoker.invoke(null, observable.getValue());

        return invoker;
    }

    @Contract("null, null -> fail")
    private static <T> Runnable install(final ObservableValue<T> observable, ChangeListener<T> cl) {
        observable.addListener(cl);
        return () -> observable.removeListener(cl);
    }

    private Listener() {
        throw new UnsupportedOperationException("Can not instantiate utility class");
    }

}
