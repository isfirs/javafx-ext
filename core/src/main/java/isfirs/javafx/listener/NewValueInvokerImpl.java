package isfirs.javafx.listener;

import java.util.function.Consumer;

/**
 * @author Isfirs
 */
final class NewValueInvokerImpl<T> extends UnbinderImpl implements NewValueInvoker<T> {

    private final Consumer<T> callback;

    NewValueInvokerImpl(final Runnable disposer, Consumer<T> callback) {
        super(disposer);
        this.callback = callback;
    }

    @Override
    public void invoke(final T newValue) {
        callback.accept(newValue);
    }

}
