package isfirs.javafx.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.isNull;

/**
 * @param <M> The may-be type.
 */
public final class Maybe<M> {

    private static Maybe<?> NOTHING;

    public static <M> Maybe<M> nothing() {
        if (isNull(NOTHING)) {
            // Lazy init
            NOTHING = new Maybe<>();
        }

        @SuppressWarnings("unchecked")
        Maybe<M> nothing = ((Maybe<M>) NOTHING);
        return nothing;
    }

    @Contract("null -> fail")
    public static <M> Maybe<M> is(@Nullable M value) {
        if (isNull(value))
            requireValue();
        return new Maybe<>(value);
    }

    public static <M> Maybe<M> ofNullable(@Nullable M value) {
        return isNull(value) ? nothing() : is(value);
    }

    private static <M> M requireNonNull(String property, @Nullable M value) {
        if (isNull(value))
            throw new NullPointerException(property + " is null");

        return value;
    }

    private static void requireValue() {
        throw new NoSuchElementException("No value");
    }

    // ---

    private final M value;

    private Maybe() {
        value = null;
    }

    private Maybe(final M value) {
        this.value = value;
    }

    public boolean exists() {
        return value != null;
    }

    public boolean absent() {
        return value == null;
    }

    // Consumers

    public void ifExists(final Consumer<? super M> consumer) {
        if (absent())
            return;
        requireNonNull("Consumer", consumer).accept(value);
    }

    public void ifAbsent(final Runnable consumer) {
        if (absent())
            consumer.run();
    }

    public void ifExistsOrElse(final Consumer<? super M> ifExists, final Runnable ifAbsent) {
        if (exists())
            ifExists.accept(value);
        else
            ifAbsent.run();
    }

    // Filters

    public Maybe<M> filter(final Predicate<M> predicate) {
        requireNonNull("Predicate", predicate);
        if (absent())
            return this;
        else
            return predicate.test(value) ? this : nothing();
    }

    // Mappers

    /**
     * @param mapper
     * @param <O>    The other type
     * @return
     */
    public <O> Maybe<O> map(final Function<? super M, ? extends O> mapper) {
        requireNonNull("Mapper", mapper);
        if (absent())
            return nothing();
        else
            return ofNullable(mapper.apply(value));
    }

    /**
     * @param mapper
     * @param <O>    The other type
     * @return
     */
    public <O> Maybe<O> flatMap(Function<? super M, Maybe<O>> mapper) {
        requireNonNull("Mapper", mapper);
        if (absent())
            return nothing();
        else
            return requireNonNull("Mapper value", mapper.apply(value));
    }

    public <O> Maybe<O> flatMapOrInstead(Function<? super M, Maybe<O>> mapper, Function<? super M, O> instead) {
        requireNonNull("Mapper", mapper);
        requireNonNull("Instead", instead);
        if (absent())
            return nothing();
        else {

            final Maybe<O> apply = requireNonNull("Mapper value", mapper.apply(value));
            if (apply.absent())
                return is(instead.apply(value));
            else
                return requireNonNull("Mapper value", mapper.apply(value));
        }
    }

    public Maybe<M> assume(Supplier<? extends M> factory) {
        if (exists())
            return this;
        else
            return is(factory.get());
    }

    // Getters

    public M get() {
        if (absent())
            throw new NoSuchElementException("No value");

        return value;
    }

    public M or(final Supplier<? extends M> or) {
        if (exists())
            return value;
        else
            return requireNonNull("Or", or).get();
    }

    public M instead(M instead) {
        if (exists())
            return value;
        else
            return instead;
    }

    /**
     * @param throwable
     * @param <T>       Throwable type.
     * @return
     * @throws T
     */
    public <T extends Throwable> M insteadThrow(final Supplier<T> throwable) throws T {
        if (exists())
            return value;
        else
            throw requireNonNull("Throwable", throwable).get();
    }

    //

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;

        if (!(obj instanceof Maybe))
            return false;

        Maybe<?> other = (Maybe<?>) obj;
        return Objects.equals(value, other.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        if (exists()) {
            return String.format("Maybe{%s}", value);
        } else {
            return "Maybe{nothing}";
        }
    }

}
