package isfirs.javafx.util;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;

/**
 * @author Isfirs
 */
public final class BorderHelper {

    private static final Border RED = new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT));

    public static void red(final Region region) {
        region.setBorder(RED);
    }

    public static void clear(final Region region) {
        region.setBorder(null);
    }

}
