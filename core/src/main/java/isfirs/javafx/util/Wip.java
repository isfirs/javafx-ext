package isfirs.javafx.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Work in progress.
 *
 * Classes or methods annotated with this annotations are considered "unfinished", as they are untested.
 *
 * @author Isfirs
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.SOURCE)
public @interface Wip {
}
