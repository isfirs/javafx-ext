package isfirs.javafx.util;

import javafx.scene.Parent;
import javafx.scene.Scene;
import org.jetbrains.annotations.Contract;

import java.net.URL;
import java.util.function.Consumer;

/**
 * @author Isfirs
 */
public final class FxUtil {

    @Contract("null, null, null -> fail")
    public static <T> void addStyleSheet(final Scene scene, final Class<T> locator, final String cssFile) {
        addStyleSheet(scene.getStylesheets()::add, locator.getResource(cssFile));
    }

    @Contract("null, null, null -> fail")
    public static <T> void addStyleSheet(final Parent node, final Class<T> locator, final String cssFile) {
        addStyleSheet(node.getStylesheets()::add, locator.getResource(cssFile));
    }

    @Contract("null, null -> fail")
    public static <T> void addStyleSheet(final Scene scene, final Class<T> locator) {
        addStyleSheet(scene, locator, buildCssFileName(locator));
    }

    @Contract("null, null -> fail")
    public static <T> void addStyleSheet(final Parent node, final Class<T> locator) {
        addStyleSheet(node, locator, buildCssFileName(locator));
    }

    private static <T> String buildCssFileName(final Class<T> locator) {
        return String.format("%s.css", locator.getSimpleName());
    }

    private static void addStyleSheet(Consumer<String> cssTarget, final URL cssFile) {
        cssTarget.accept(cssFile.toExternalForm());
    }

    private FxUtil() {
        throw new UnsupportedOperationException("Utility constructor");
    }

}
