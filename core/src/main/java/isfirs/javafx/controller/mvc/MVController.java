package isfirs.javafx.controller.mvc;

import isfirs.javafx.annotations.OnStop;
import isfirs.javafx.controller.Controller;
import javafx.scene.Node;

import javax.annotation.Nullable;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Isfirs
 */
public abstract class MVController<M extends Model<?>, N extends Node, V extends View<M, N>> extends Controller<N> {

    private M model;
    private V view;

    protected MVController(@Nullable Controller<? extends Node> parent) {
        super(parent);
    }

    @Override
    protected final N createNode() {
        model = newModelFactory().get();
        view = newViewFactory().apply(model);

        return view.node();
    }

    @OnStop
    private void onStop() {
        model.dispose();
        model = null;
        view = null;
    }

    //

    protected abstract Supplier<M> newModelFactory();

    protected abstract Function<M, V> newViewFactory();

    //

    protected final M model() {
        return model;
    }

    protected final V view() {
        return view;
    }

}
