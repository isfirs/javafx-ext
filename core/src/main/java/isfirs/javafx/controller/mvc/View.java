package isfirs.javafx.controller.mvc;

import isfirs.javafx.controller.util.FxmlUtil;
import javafx.scene.Node;

import java.net.URL;
import java.util.Objects;

import static java.util.Objects.isNull;

/**
 * @author Isfirs
 */
public abstract class View<M extends Model<?>, N extends Node> {

    private boolean isDisposed = false;
    private M model;
    private N node;

    protected View(final M model) {
        this.model = Objects.requireNonNull(model);
    }

    protected abstract N createNode();

    public synchronized final void dispose() {
        if (isDisposed)
            throw new IllegalStateException();

        model.dispose();
        model = null;
        node = null;
        isDisposed = true;
    }

    public final M model() {
        return model;
    }

    public final N node() {
        if (isDisposed)
            throw new IllegalStateException();
        if (isNull(node))
            node  = createNode();
        return node;
    }

    protected final N loadFromFxml() {
        final String fxmlResource = String.format("%s.fxml", getClass().getSimpleName());
        final URL location = getClass().getResource(fxmlResource);
        return loadFromFxml(location);
    }

    protected final N loadFromFxml(final URL location) {
        return FxmlUtil.loadFxml(location, c -> this);
    }

}
