package isfirs.javafx.controller.mvc;

/**
 * @author Isfirs
 */
public abstract class Model<VO> {

    protected Model() {
        super();
    }

    public void dispose() {
        //
    }

    public final void set(VO data) {
        if (data == null) {
            clear();
        } else {
            put(data);
        }
    }

    protected abstract void put(VO data);

    protected abstract void clear();

}
