package isfirs.javafx.controller;

import javafx.scene.Node;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.function.Consumer;

/**
 * @author Isfirs
 */
final class Util {

    static <C extends Controller<? extends Node>> void invoke(C controller, Method method) {
        final boolean wasAccessible = method.isAccessible();
        try {
            method.setAccessible(true);
            method.invoke(controller);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            method.setAccessible(wasAccessible);
        }
    }

    private Util() {
        throw new UnsupportedOperationException("Utility constructor");
    }

}
