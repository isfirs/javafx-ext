package isfirs.javafx.controller;

import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * @author Isfirs
 */
public abstract class RootController<T extends Parent> extends Controller<T> {

    protected RootController() {
        super(null);
    }

    public final Scene asScene() {
        return new Scene(getNode());
    }

    public final Scene asScene(final double width, final double height) {
        return new Scene(getNode(), width, height);
    }

}
