package isfirs.javafx.controller;

import javafx.application.Platform;

/**
 * @author Isfirs
 */
public final class FxPlatform {

    private static final String FX_CONCURRENT_THREAD_GROUP_NAME = "javafx concurrent thread pool";

    public static boolean isFxMainThread() {
        return Platform.isFxApplicationThread();
    }

    public static boolean isAnyFxThread() {
        boolean isFxThread = isFxMainThread();

        final ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
        if (!isFxThread && threadGroup != null) {
            isFxThread = threadGroup.getName().equals(FX_CONCURRENT_THREAD_GROUP_NAME);
        }
        return isFxThread;
    }

    private FxPlatform() {
        throw new UnsupportedOperationException("Can not instantiate utility class");
    }
}
