package isfirs.javafx.controller.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.util.Callback;

import java.net.URL;

/**
 * @author Isfirs
 */
public final class FxmlUtil {

    public static <N extends Node> N loadFxml(final URL location, final Callback<Class<?>, Object> controllerFactory) {
        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setControllerFactory(controllerFactory);
        try {
            return fxmlLoader.load();
        } catch (Exception e) {
            throw new RuntimeException("Exception loading FMXL", e);
        }
    }

    private FxmlUtil() {
        throw new UnsupportedOperationException("Utility constructor");
    }
}
