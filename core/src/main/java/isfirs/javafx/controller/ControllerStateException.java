package isfirs.javafx.controller;

import javafx.scene.Node;

import java.util.StringJoiner;

/**
 * @author Isfirs
 */
public class ControllerStateException extends RuntimeException {

    public ControllerStateException(Controller<? extends Node> controller) {

    }

    public ControllerStateException(Controller<? extends Node> controller, ControllerState state) {
        super(buildMessage(controller, state));
    }

    private static String buildMessage(Controller<? extends Node> controller, ControllerState state) {
        final StringJoiner sb = new StringJoiner(" ");
        switch (state) {
            case NOT_STARTED:
                sb.add(controller.getClass().getSimpleName())
                        .add("is not started");
                break;
            case STARTING:
                break;
            case STARTED:
                break;
            case STOPPING:
                break;
            case STOPPED:
                break;
        }

        return sb.toString();
    }

}
