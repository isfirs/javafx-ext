package isfirs.javafx.controller;

import isfirs.javafx.annotations.ChildController;
import isfirs.javafx.annotations.OnStart;
import isfirs.javafx.annotations.OnStop;
import isfirs.javafx.annotations.RunLater;
import isfirs.javafx.controller.util.FxmlUtil;
import isfirs.javafx.util.Maybe;
import javafx.application.Platform;
import javafx.scene.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

/**
 * @author Isfirs
 */
public abstract class Controller<N extends Node> {

    // Get Logger of implementing class
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Nullable
    private final Controller<? extends Node> parent;

    private final List<Controller<? extends Node>> children = new ArrayList<>();

    private final List<Runnable> childControllerStartMethods = new ArrayList<>();
    private final List<Runnable> onStartMethods = new ArrayList<>();
    private final List<Runnable> onStopMethods = new ArrayList<>();
    private final List<Runnable> runLaterMethods = new ArrayList<>();

    private boolean started = false;

    private N node;

    protected Controller(@Nullable final Controller<? extends Node> parent) {
        if (!(this instanceof RootController) && parent == null) {
            throw new IllegalArgumentException("Only root controller can have null parent");
        }
        this.parent = parent;

        setupCallbackMethods();
        setupFields();
    }

    private void setupCallbackMethods() {
        Class<?> nextClass = getClass();
        while (nextClass != null) {

            final Method[] methods = nextClass.getDeclaredMethods();
            for (final Method method : methods) {

                final List<? extends Annotation> annotations = Stream.of(OnStart.class, OnStop.class, RunLater.class)
                        .map(method::getAnnotation)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                if (annotations.isEmpty()) {
                    logger.debug("Method has no callback annotations, skipping.");
                    continue;
                } else if (annotations.size() > 1) {
                    logger.warn("Method {} has more than 1 callback annotation, skipping", method.getName());
                    continue;
                }

                // Not allowed on static
                if (Modifier.isStatic(method.getModifiers())) {
                    logger.warn("static not allowed on method {}", method.getName());
                    continue;
                }

                final Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length != 0) {
                    logger.warn("Callbacks must have no arguments");
                    continue;
                }
                final Class<?> returnType = method.getReturnType();
                if (returnType != Void.TYPE) {
                    logger.warn("Callbacks must have a void return type");
                    continue;
                }

                final Class<? extends Annotation> annotation = annotations.get(0).annotationType();
                final Runnable invoker = () -> Util.invoke(this, method);

                if (annotation == OnStart.class)
                    onStartMethods.add(invoker);
                else if (annotation == OnStop.class)
                    onStopMethods.add(invoker);
                else if (annotation == RunLater.class)
                    runLaterMethods.add(invoker);
                else
                    throw new IllegalArgumentException("Annotation class is not mapped");

            }
            nextClass = nextClass.getSuperclass();
        }
    }

    private void setupFields() {
        Class<?> nextClass = getClass();
        while (nextClass != null) {

            final Field[] methods = nextClass.getDeclaredFields();
            for (final Field field : methods) {

                final ChildController annotation = field.getAnnotation(ChildController.class);

                if (isNull(annotation)) {
                    logger.debug("Field has no annotation, skipping.");
                    continue;
                }

                // Not allowed on static
                if (Modifier.isStatic(field.getModifiers())) {
                    logger.warn("static not allowed on field {}", field.getName());
                    continue;
                }

                final Class<?> fieldType = field.getType();
                if (!Controller.class.isAssignableFrom(fieldType)) {
                    logger.warn("Field must be a Controller subtype");
                    continue;
                }

                final Runnable invoker = () -> {
                    final boolean wasAccessible = field.isAccessible();
                    try {
                        field.setAccessible(true);
                        final Controller<? extends Node> controller = (Controller<? extends Node>) field.get(this);
                        controller.start();
                    } catch (Exception e) {
                        final String msg = "Caught exception setting up field initializers";
                        logger.error(msg);
                        throw new RuntimeException(msg, e);
                    } finally {
                        field.setAccessible(wasAccessible);
                    }
                };
                childControllerStartMethods.add(invoker);
            }
            nextClass = nextClass.getSuperclass();
        }
    }

    private void registerChild(final Controller<? extends Node> child) {
        synchronized (children) {
            logger.debug("Adding child controller");
            children.add(child);
        }
    }

    public List<Controller<? extends Node>> getChildren() {
        return children;
    }

    private void deregisterChild(final Controller<? extends Node> child) {
        synchronized (children) {
            logger.debug("Removing child controller");
            children.remove(child);
        }
    }

    public Maybe<Controller<? extends Node>> getParent() {
        return Maybe.ofNullable(parent);
    }

    public final N getNode() throws ControllerStateException {
        if (!started) {
            throw new ControllerStateException(this, ControllerState.NOT_STARTED);
        }

        return node;
    }

    public synchronized final void start() throws ControllerStateException {
        if (!FxPlatform.isAnyFxThread())
            throw new IllegalStateException("Not on any FX Thread");

        if (isStarted())
            throw new ControllerStateException(this, ControllerState.STARTED);

        childControllerStartMethods.forEach(Runnable::run);

        try {
            preStart();
        } catch (Exception e) {
            logger.error("Exception during preStart", e);
        }

        try {
            node = createNode();
            if (node == null)
                throw new IllegalStateException("node is null");
        } catch (Exception e) {
            logger.warn("Exception during createNode");
            throw new RuntimeException(e);
        }

        getParent().ifExists(parent -> parent.registerChild(this));

        try {
            onStartMethods.forEach(Runnable::run);
        } catch (Exception e) {
            //
            node = null;
            logger.warn("Exception during onStart()");
            throw new RuntimeException(e);
        }

        runLaterMethods.forEach(Platform::runLater);

        started = true;
    }

    public synchronized final void stop() throws ControllerStateException, StopInterruptedException {
        if (!isStarted())
            throw new ControllerStateException(this);

        if (!FxPlatform.isAnyFxThread())
            throw new IllegalStateException("Not on any FX Thread");

        // TODO Ask if stop is possible

        node = null;

        Stream.of(children.toArray(new Controller[]{})).forEach(Controller::stop);

        getParent().ifExists(parent -> parent.deregisterChild(this));

        try {
            onStopMethods.forEach(Runnable::run);
            started = false;
        } catch (StopInterruptedException e) {
            logger.debug("Controller intercepted stop call");
            throw e;
        } catch (Exception e) {
            logger.warn("Unhandled exception during {}::onStop()", getClass().getSimpleName(), e);
            started = false;
        }
    }

    /**
     * Allows internal tear-down and reboot of controller instances.
     *
     * @throws ControllerStateException
     */
    protected final void restart() throws ControllerStateException {
        synchronized (this) {
            if (!FxPlatform.isAnyFxThread()) {
                throw new IllegalStateException("Not on any FX Thread");
            }

            stop();

            start();
        }
    }

    // ---

    protected void preStart() {

    }

    protected final N loadFromFxml() {
        final String fxmlResource = String.format("%s.fxml", getClass().getSimpleName());
        final URL location = getClass().getResource(fxmlResource);
        return loadFromFxml(location);
    }

    protected final N loadFromFxml(final URL location) {
        return FxmlUtil.loadFxml(location, c -> this);
    }

    // ---

    protected abstract N createNode();

    public final boolean isStarted() {
        return started;
    }

}
