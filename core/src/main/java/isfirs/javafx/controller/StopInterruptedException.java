package isfirs.javafx.controller;

/**
 * @author Isfirs
 */
public class StopInterruptedException extends RuntimeException {

    public StopInterruptedException() {
        super();
    }

}
