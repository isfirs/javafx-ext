package isfirs.javafx.controller;

/**
 * @author Isfirs
 */
public enum ControllerState {

    NOT_STARTED,

    STARTING,

    STARTED,

    STOPPING,

    STOPPED;

}
