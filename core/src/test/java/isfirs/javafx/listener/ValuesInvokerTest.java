package isfirs.javafx.listener;

import org.junit.Test;

public final class ValuesInvokerTest extends InvokerTestBase {

    @Test
    public void testValuesListener() {
        Listener.onValues(boolProperty, (oldValue, newValue) -> wasChanged.set(true));
        boolProperty.setValue(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testValuesInvoker() {
        final ValuesInvoker<Boolean> invoker = Listener.onValues(boolProperty, (oldValue, newValue) -> wasChanged.set(true));
        invoker.invoke(false, true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testValuesInvokerRunsOnce() {
        Listener.onValues(boolProperty, (oldValue, newValue) -> wasChanged.set(true), true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

}
