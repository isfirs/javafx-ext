package isfirs.javafx.listener;

import org.junit.Test;

public final class NewValueInvokerTest extends InvokerTestBase {

    @Test
    public void testNewValueListener() {
        Listener.onNewValue(boolProperty, (newValue) -> wasChanged.set(true));
        boolProperty.setValue(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testNewValueInvoker() {
        final NewValueInvoker<Boolean> invoker = Listener.onNewValue(boolProperty, (newValue) -> wasChanged.set(true));
        invoker.invoke(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testNewValueInvokerRunsOnce() {
        Listener.onNewValue(boolProperty, (newValue) -> wasChanged.set(true), true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

}
