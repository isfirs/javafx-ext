package isfirs.javafx.listener;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.BooleanPropertyBase;
import org.assertj.core.api.WithAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Test base to simplify execution.
 *
 * @author Isfirs
 */
@Ignore
abstract class InvokerTestBase implements WithAssertions {

    protected BooleanProperty boolProperty;
    protected AtomicBoolean wasChanged;

    @Before
    public void setup() {
        boolProperty = new BooleanPropertyBase(false) {
            @Override
            public Object getBean() {
                return InvokerTestBase.this;
            }

            @Override
            public String getName() {
                return "testBooleanProperty";
            }
        };

        wasChanged = new AtomicBoolean(false);
    }

    @After
    public void after() {
        boolProperty = null;
        wasChanged = null;
    }

}
