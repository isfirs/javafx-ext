package isfirs.javafx.listener;

import org.junit.Test;

public final class VoidInvokerTests extends InvokerTestBase {

    @Test
    public void testVoidListener() {
        Listener.onChange(boolProperty, () -> wasChanged.set(true));
        boolProperty.setValue(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testVoidInvoker() {
        final VoidInvoker invoker = Listener.onChange(boolProperty, () -> wasChanged.set(true));
        invoker.invoke();

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testVoidInvokerRunsOnce() {
        Listener.onChange(boolProperty, () -> wasChanged.set(true), true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

}
