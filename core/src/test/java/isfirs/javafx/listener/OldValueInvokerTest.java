package isfirs.javafx.listener;

import org.junit.Test;

public final class OldValueInvokerTest extends InvokerTestBase {

    @Test
    public void testOldValueListener() {
        Listener.onOldValue(boolProperty, (oldValue) -> wasChanged.set(true));
        boolProperty.setValue(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testOldValueInvoker() {
        final OldValueInvoker<Boolean> invoker = Listener.onOldValue(boolProperty, (oldValue) -> wasChanged.set(true));
        invoker.invoke(true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

    @Test
    public void testOldValueInvokerRunsOnce() {
        Listener.onNewValue(boolProperty, (newValue) -> wasChanged.set(true), true);

        //
        assertThat(wasChanged.get()).isTrue();
    }

}
