package isfirs.javafx.util;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class MaybeTests {

    // Factory tests

    /**
     * Test that a {@link Maybe} contains a value and the provided value.
     */
    @Test
    public void testIsFactory() {
        final Object o = new Object();
        final Maybe<Object> maybe = Maybe.is(o);

        assertThat(maybe.exists())
                .isTrue();
        assertThat(maybe.get())
                .isSameAs(o);
    }

    @Test
    public void testIsFactoryRequiresValue() {
        final Throwable thrown = catchThrowable(() -> Maybe.is(null));

        assertThat(thrown)
                .isInstanceOf(NoSuchElementException.class)
                .hasNoCause()
                .hasMessage("No value");
    }

    /**
     * Ensure the first return instance of {@link Maybe#nothing()} is the same as any subsequent call.
     */
    @Test
    public void testNothingIsSingleton() {
        final Maybe<Object> nothing = Maybe.nothing();

        assertThat(nothing)
                .isNotNull()
                .isEqualTo(Maybe.nothing());
    }

    @Test
    public void testNothingIsEmpty() {
        final Maybe<Object> nothing = Maybe.nothing();

        assertThat(nothing.exists())
                .isFalse();
        assertThat(nothing.absent())
                .isTrue();
    }

    // General tests

    /**
     * Test that an absent value throws an Exception.
     */
    @Test
    public void testExceptionOnNothingGet() {
        final Maybe<Object> nothing = Maybe.nothing();

        final Throwable thrown = catchThrowable(nothing::get);

        assertThat(thrown)
                .isInstanceOf(NoSuchElementException.class)
                .hasNoCause()
                .hasMessage("No value");
    }

    // Mapper tests

    /**
     * If absent, {@link Maybe#assume(Supplier)} should return a Maybe instance with the produced value.
     */
    @Test
    public void testAssume() {
        final Maybe<Object> nothing = Maybe.nothing();
        assertThat(nothing.absent()).isTrue();

        final Object assumption = new Object();

        final Object maybe = nothing.assume(() -> assumption).get();

        assertThat(maybe)
                .isSameAs(assumption);
    }

    @Test
    public void testMap() {
        final int[] ints = {1};

        final Maybe<Integer> maybe = Maybe.is(ints)
                .map(array -> array[0]);

        assertThat(maybe.get())
                .isSameAs(1);
    }

    // Filter tests

    @Test
    public void testFilterWillHoldvalue() {
        final int[] ints = {1};

        final Maybe<int[]> maybe = Maybe.is(ints)
                .filter(array -> array.length == 1);

        assertThat(maybe.exists())
                .isTrue();

        assertThat(maybe.get())
                .isSameAs(ints);
    }

    @Test
    public void testFilterWillRemoveValue() {
        final int[] ints = {1};

        final Maybe<int[]> maybe = Maybe.is(ints)
                .filter(array -> array.length == 2);

        assertThat(maybe.exists())
                .isFalse();
    }

    // Getter tests

    @Test
    public void testIfExists() {
        final Object o = new Object();
        final Maybe<Object> maybe = Maybe.is(o);

        final AtomicBoolean ref = new AtomicBoolean(false);
        maybe.ifExists(val -> ref.set(true));

        assertThat(ref.get())
                .isTrue();
    }

    @Test
    public void testIfAbsent() {
        final Maybe<Object> maybe = Maybe.nothing();

        final AtomicBoolean ref = new AtomicBoolean(false);
        maybe.ifAbsent(() -> ref.set(true));

        assertThat(ref.get())
                .isTrue();
    }

    /**
     * A filled Maybe should not use the <code>instead</code> value, but return the original value.
     */
    @Test
    public void testInstead() {
        final Object is = new Object();
        final Object instead = new Object();

        //
        final Maybe<Object> maybe = Maybe.is(is);
        final Object result = maybe.instead(instead);

        // Must
        assertThat(result)
                .isNotEqualTo(instead);

        assertThat(maybe.get())
                .isNotNull()
                .isSameAs(is);
    }

    /**
     * An instance created by {@link Maybe#nothing()} should use its <code>instead</code> value.
     */
    @Test
    public void testNothingReturnsInstead() {
        final Object instead = new Object();

        final Maybe<Object> nothing = Maybe.nothing();
        final Object result = nothing.instead(instead);

        assertThat(result)
                .isSameAs(instead);
    }

    @Test
    public void testIsReturnsNotOr() {
        final Object original = new Object();
        final Maybe<Object> maybe = Maybe.is(original);

        final Object other = new Object();
        final Object result = maybe.or(() -> other);

        assertThat(result)
                .isSameAs(original);
    }

    @Test
    public void testNothingReturnsOr() {
        final Maybe<Object> maybe = Maybe.nothing();

        final Object other = new Object();
        final Object result = maybe.or(() -> other);

        assertThat(result)
                .isSameAs(other);
    }

    @Test
    public void testThrow() {
        final Maybe<Object> nothing = Maybe.nothing();

        final Throwable thrown = catchThrowable(() -> nothing.insteadThrow(RuntimeException::new));

        assertThat(thrown)
                .isInstanceOf(RuntimeException.class)
                .hasNoCause();
    }

    @Test
    public void testNoThrow() {
        final Object original = new Object();
        final Maybe<Object> nothing = Maybe.is(original);

        final Throwable thrown = catchThrowable(() -> {
            final Object result = nothing.insteadThrow(RuntimeException::new);

            assertThat(result)
                    .as("Check result")
                    .isEqualTo(original);
        });

        assertThat(thrown)
                .isNull();
    }

    /**
     * Less a test, more an example how to go from Optional to Maybe.
     */
    @Test
    public void exampleOptionalToMaybe() {
        final Object original = new Object();

        final Object maybe = Optional.of(original)
                // These 2 lines are neccessary to convert from a Optional-chain to a Maybe-chain
                .map(Maybe::is)
                .orElseGet(Maybe::nothing)
                //
                .assume(() -> new Object()) // Will not be executed
                .get();

        assertThat(maybe)
                .isSameAs(original);
    }

}
