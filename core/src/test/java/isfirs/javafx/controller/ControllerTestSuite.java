package isfirs.javafx.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        FxPlatformTests.class,
        UiTest.class,
        ControllerTests.class,
        ControllerHierarchyTests.class
})
public class ControllerTestSuite {
}
