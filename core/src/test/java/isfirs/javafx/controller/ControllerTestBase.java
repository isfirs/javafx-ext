package isfirs.javafx.controller;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.assertj.core.api.WithAssertions;
import org.testfx.framework.junit.ApplicationTest;

abstract class ControllerTestBase extends ApplicationTest implements WithAssertions {

    protected RootController<BorderPane> rootController;

    @Override
    public void init() throws Exception {
        rootController = new TestRootController();
    }

    @Override
    public void start(Stage stage) throws Exception {
        rootController.start();
    }

    @Override
    public void stop() throws Exception {
        rootController.stop();
        rootController = null;
    }

}
