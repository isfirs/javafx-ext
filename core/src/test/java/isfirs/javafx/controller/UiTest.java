package isfirs.javafx.controller;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.junit.Test;

public class UiTest extends ControllerTestBase {

    private Node node;
    private Controller<Node> controller;

    @Override
    public void init() throws Exception {
        super.init();

        node = new BorderPane();
        controller = new Controller<Node>(rootController) {
            @Override
            protected Node createNode() {
                return node;
            }
        };
    }

    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);

        controller.start();
    }

    @Override
    public void stop() throws Exception {
        super.stop();

        // Just to be sure!
        assertThat(controller.isStarted()).isFalse();
        controller = null;
    }

    @Test
    public void testNodeExists() {
        // Should be that instance
        assertThat(controller.getNode()).isSameAs(node);
    }

    @Test
    public void testControllerIsStarted() {
        assertThat(controller.isStarted()).isTrue();
    }

}
