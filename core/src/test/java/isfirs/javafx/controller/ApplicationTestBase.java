package isfirs.javafx.controller;

import org.assertj.core.api.WithAssertions;
import org.testfx.framework.junit.ApplicationTest;

abstract class ApplicationTestBase extends ApplicationTest implements WithAssertions {

}
