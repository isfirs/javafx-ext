package isfirs.javafx.controller;

import javafx.scene.layout.BorderPane;

final class TestRootController extends RootController<BorderPane> {

    TestRootController() {
        super();
    }

    @Override
    protected BorderPane createNode() {
        return new BorderPane();
    }

}
