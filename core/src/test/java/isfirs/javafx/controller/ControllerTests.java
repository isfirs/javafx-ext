package isfirs.javafx.controller;

import isfirs.javafx.util.Maybe;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import org.assertj.core.api.WithAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ControllerTests implements WithAssertions {

    private RootController<BorderPane> rootController;

    @Before
    public void setUp() throws Exception {
        rootController = new TestRootController();
    }

    @After
    public void tearDown() throws Exception {
        rootController = null;
    }

    // Tests

    @Test
    public void rootControllerRequiresNoParent() {
        final Throwable thrown = catchThrowable(() -> new RootController<Parent>() {
            @Override
            protected Parent createNode() {
                throw new UnsupportedOperationException("Will not be called during test");
            }
        });

        assertThat(thrown).isNull();
    }

    @Test
    public void parentCanNotBeNull() {
        final Throwable thrown = catchThrowable(() -> new Controller<Node>(null) {
            @Override
            protected Node createNode() {
                throw new UnsupportedOperationException("Will not be called during test");
            }
        });

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Only root controller can have null parent");
    }

    @Test
    public void newControllerIsNotStarted() {
        final Controller<Node> controller = new Controller<Node>(rootController) {
            @Override
            protected Node createNode() {
                throw new UnsupportedOperationException("Will not be called during test");
            }
        };

        assertThat(controller.isStarted()).isFalse();
    }

    @Test
    public void controllerIsNotStarted() {
        final Controller<Node> testController = new Controller<Node>(rootController) {
            @Override
            protected Node createNode() {
                throw new UnsupportedOperationException("Will not be called during test");
            }
        };

        final Throwable thrown = catchThrowable(() -> {
            // This should throw the Exception
            testController.getNode();
        });

        assertThat(thrown)
                .isInstanceOf(ControllerStateException.class)
                .hasNoCause()
                .hasMessage(String.format("%s is not started", testController.getClass().getSimpleName()));
    }

    @Test
    public void parentIsRootController() {
        final Controller<Node> controller = new Controller<Node>(rootController) {
            @Override
            protected Node createNode() {
                throw new UnsupportedOperationException("Will not be called during test");
            }
        };

        final Maybe<Controller<? extends Node>> mayParent = controller.getParent();
        assertThat(mayParent.exists());

        final Controller<? extends Node> parentController = mayParent.get();
        assertThat(parentController)
                .isInstanceOf(RootController.class)
                .isSameAs(rootController);
    }

    @Test
    public void canNotStartIfNonFxThread() {
        final Controller<Node> controller = new Controller<Node>(rootController) {
            @Override
            protected Node createNode() {
                 throw new UnsupportedOperationException("Will not be called during test");
            }
        };

        final Throwable thrown = catchThrowable(() -> {
            // This does not run on FX Thread, exception is thrown
            controller.start();
        });

        assertThat(thrown)
                .isInstanceOf(IllegalStateException.class)
                .hasNoCause()
                .hasMessage("Not on any FX Thread");
    }

}
