package isfirs.javafx.controller;

import isfirs.javafx.annotations.ChildController;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.WithAssertions;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import javax.annotation.Nullable;
import java.util.stream.Stream;

public class ControllerHierarchyTests extends ApplicationTest implements WithAssertions {

    private TestRootController rootController;

    @Override
    public void start(Stage stage) throws Exception {
        rootController = new TestRootController();
        rootController.start();
    }

    @Override
    public void stop() throws Exception {
        rootController.stop();

        // Must all be false
        Stream.of(
                rootController.isStarted(),
                rootController.childController.isStarted()
        ).map(Assertions::assertThat).forEach(AbstractBooleanAssert::isFalse);

        final Throwable thrown = catchThrowable(() -> rootController.childController.getNode());
        assertThat(thrown)
                .isInstanceOf(ControllerStateException.class);
    }

    @Test
    public void childControllerIsRegistered() {
        assertThat(rootController.getChildren())
                .hasSize(1)
                .containsOnly(rootController.childController);
    }

    @Test
    public void test() {

        // Must all be true
        Stream.of(
                rootController.isStarted(),
                rootController.childController.isStarted()
        ).map(Assertions::assertThat).forEach(AbstractBooleanAssert::isTrue);
    }

    // region Inner Types

    private static class TestRootController extends RootController<BorderPane> {

        @ChildController
        private final Controller<Node> childController = new TestController(this);

        protected TestRootController() {
            super();
        }

        @Override
        protected BorderPane createNode() {
            return new BorderPane();
        }
    }

    private static class TestController extends Controller<Node> {

        protected TestController(@Nullable Controller<? extends Node> parent) {
            super(parent);
        }

        @Override
        protected Node createNode() {
            return new Pane();
        }
    }

}
