package isfirs.javafx.examples.async;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class AsyncApplicationImpl extends Application implements AsynApplication {

    private Stage primaryStage;

    public static void main(final String... args) {
        Application.launch(AsyncApplicationImpl.class, args);
    }

    @Override
    public void start(final Stage primaryStage) {
        this.primaryStage = primaryStage;

        // Step 1: Instantiate
        final AsyncAppRootController rootController = new AsyncAppRootController(this);

        // STep 2: Start up the controller
        rootController.start();

        //Step 3: Create scene and set it into stage
        final Scene scene = new Scene(rootController.getNode());
        primaryStage.setScene(scene);

        // Step 4: Show stage
        primaryStage.show();
    }

    @Override
    public void setTitle(final String title) {
        primaryStage.setTitle(title);
    }
}
