package isfirs.javafx.examples.async;

import isfirs.javafx.controller.Controller;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

final class AsyncUseCaseController extends Controller<BorderPane> {

    private static final Object MONITOR = new Object();

    private final double delay;

    AsyncUseCaseController(final AsyncAppRootController parent, final double delay) {
        super(parent);

        this.delay = delay;
    }

    @Override
    protected BorderPane createNode() {

        final long delay = (long) Math.floor(this.delay * 1000d);
        try {
            synchronized (MONITOR) {
                MONITOR.wait(delay);
            }
        } catch (InterruptedException e) {
            //
        }

        final String label = String.format("Controller started in %sms", delay);

        return new BorderPane(new Label(label));
    }

}
