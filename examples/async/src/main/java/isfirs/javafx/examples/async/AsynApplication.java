package isfirs.javafx.examples.async;

interface AsynApplication {

    /**
     * Set the title on the primary stage.
     *
     * @param title The new stage title
     */
    void setTitle(String title);

}
