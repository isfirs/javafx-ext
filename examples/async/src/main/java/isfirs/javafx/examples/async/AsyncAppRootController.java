package isfirs.javafx.examples.async;

import isfirs.javafx.annotations.OnStart;
import isfirs.javafx.controller.Controller;
import isfirs.javafx.controller.RootController;
import isfirs.javafx.listener.Listener;
import javafx.animation.FadeTransition;
import javafx.animation.Transition;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

import static java.util.Objects.nonNull;

final class AsyncAppRootController extends RootController<BorderPane> {

    private static final FadeTransition FADE_IN;
    private static final FadeTransition FADE_OUT;

    static {
        final FadeTransition fadeOut = new FadeTransition();
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        FADE_OUT = fadeOut;

        final FadeTransition fadeIn = new FadeTransition();
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        FADE_IN = fadeIn;
    }

    private final AsynApplication application;

    private final ObjectProperty<Controller<? extends Node>> currentController = new SimpleObjectProperty<>(this, "currentController");

    /**
     * @param application A reference to the application
     */
    protected AsyncAppRootController(final AsynApplication application) {
        super();
        this.application = application;
    }

    @Override
    @NotNull
    protected BorderPane createNode() {
        final BorderPane node = new BorderPane();
        node.setPadding(new Insets(20));

        final VBox top = new VBox();
        top.setAlignment(Pos.CENTER);
        final Label instruction = new Label("Configure the slider for a delay and press the button to start a new controller asynchronous.");
        final Slider slider = new Slider();
        slider.setMin(1);
        slider.setMax(3);
        final Label amountLabel = new Label();
        amountLabel.textProperty().bind(slider.valueProperty().asString("%.2f"));

        final Button button = new Button("Start Controller");
        button.setOnAction(event -> startNewController(slider.getValue()));

        top.getChildren().addAll(instruction, slider, amountLabel, button);
        node.setTop(top);

        //
        Listener.onValues(currentController, (currController, newController) -> {
            if (nonNull(currController)) {
                synchronized (FADE_OUT) {
                    FADE_OUT.setNode(currController.getNode());
                    FADE_OUT.play();
                }
            }

            startControllerAsync(newController, button::setDisable);
        });

        return node;
    }

    private void startNewController(final double value) {
        final AsyncUseCaseController controller = new AsyncUseCaseController(this, value);
        currentController.set(controller);
    }

    @OnStart
    private void onStart() {
        application.setTitle("Simple Application example");
    }

    private void startControllerAsync(Controller<? extends Node> controller, Consumer<Boolean> setRunning) {
        final Service<Node> service = new ControllerStartService(controller);

        service.setOnFailed(event -> {
            getNode().setCenter(new Label("Service failed"));
            setRunning.accept(false);
        });
        service.setOnSucceeded(event -> {
            final Node node = service.getValue();

            synchronized (FADE_IN) {
                FADE_IN.setNode(node);
                getNode().setCenter(node);
                FADE_IN.play();

                FADE_IN.setNode(null);
                setRunning.accept(false);
            }
        });

        setRunning.accept(true);
        service.start();
    }

    private static class ControllerStartService extends Service<Node> {

        private final Controller<? extends Node> controller;

        public ControllerStartService(final Controller<? extends Node> controller) {
            this.controller = controller;
        }

        @Override
        protected Task<Node> createTask() {
            return new ControllerStartTask(controller);
        }

        private static class ControllerStartTask extends Task<Node> {

            private final Controller<? extends Node> controller;

            private ControllerStartTask(final Controller<? extends Node> controller) {
                this.controller = controller;
            }

            @Override
            protected Node call() {
                controller.start();
                return controller.getNode();
            }
        }
    }

}
