/**
 * Expect all parameters are non null by default.
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.examples.async;

import javax.annotation.ParametersAreNonnullByDefault;