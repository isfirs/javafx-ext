package isfirs.javafx.examples.startstop;

import isfirs.javafx.controller.ControllerStateException;
import isfirs.javafx.controller.FxPlatform;
import isfirs.javafx.controller.StopInterruptedException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Permission;

public final class StartStopExampleApp extends Application implements StartStopApplication, Thread.UncaughtExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(StartStopExampleApp.class);

    public static void main(final String... args) {
        Application.launch(StartStopExampleApp.class, args);
    }

    private final StartStopRootController rootController = new StartStopRootController(this);

    private Stage primaryStage;

    public StartStopExampleApp() {
        //
    }

    @Override
    public void start(final Stage primaryStage) {
        this.primaryStage = primaryStage;

        // Disable System.exit calls
        System.setSecurityManager(new ExampleSecurityManager());
        Thread.setDefaultUncaughtExceptionHandler(this);

        // Intercept primary stage closing
        primaryStage.addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, event -> {
            try {
                rootController.stop();
            } catch (StopInterruptedException e) {
                event.consume();
            }
        });

        rootController.start();

        final Scene scene = new Scene(rootController.getNode());
        primaryStage.setScene(scene);

        // Step 4: Show stage
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    // region UncaughtExceptionHandler

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        if (e instanceof StopInterruptedException) {
            // Silently discard. should not have any effect on controller states
            LOGGER.debug("Caught a StopInterruptException.");
            return;
        }

        final Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Exception caught");
        alert.setHeaderText(e.getClass().getSimpleName());

        final String message = e.getMessage() == null ? "<no message>" : e.getMessage();
        alert.setContentText(message);
        if (FxPlatform.isFxMainThread()) {
            LOGGER.info("In FX Main Thread, displaying alert");
            alert.show();
        } else {
            LOGGER.info("Not in FX Main Thread, delegating to Platform.runLater");
            Platform.runLater(alert::show);
        }
    }

    // endregion UncaughtExceptionHandler

    // region StartStopApplication

    @Override
    public void fireStop() {
        try {
            rootController.stop();
            Platform.exit();
        } catch (StopInterruptedException e) {
            // React to the controller not stopping
            throw e;
        }
    }

    @Override
    public void setTitle(final String title) {
        primaryStage.setTitle(title);
    }

    private static class ExampleSecurityManager extends SecurityManager {

        @Override
        public void checkExit(int status) {
            // Disabled System.exit calls
            throw new SecurityException("System.exit is not allowed");
        }

        @Override
        public void checkPermission(Permission perm) {
            // Allow everything
        }
    }

    // endregion StartStopApplication

}
