/**
 * Expect all parameters are non null by default.
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.examples.startstop;

import javax.annotation.ParametersAreNonnullByDefault;