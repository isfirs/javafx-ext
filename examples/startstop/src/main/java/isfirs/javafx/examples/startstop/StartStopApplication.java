package isfirs.javafx.examples.startstop;

interface StartStopApplication {

    /**
     * Set the title on the primary stage.
     *
     * @param title The new stage title
     */
    void setTitle(String title);

    void fireStop();

}
