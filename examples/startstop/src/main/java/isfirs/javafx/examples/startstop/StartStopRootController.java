package isfirs.javafx.examples.startstop;

import isfirs.javafx.annotations.OnStart;
import isfirs.javafx.annotations.OnStop;
import isfirs.javafx.controller.RootController;
import isfirs.javafx.controller.StopInterruptedException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

final class StartStopRootController extends RootController<BorderPane> {

    private final StartStopApplication application;

    private BooleanProperty isChecked = new SimpleBooleanProperty(this, "isChecked", false);

    /**
     * @param application A reference to the application
     */
    protected StartStopRootController(final StartStopApplication application) {
        super();
        this.application = application;
    }

    // region Controller

    @Override
    @NotNull
    protected BorderPane createNode() {

        final CheckBox cb = new CheckBox("Throw Exception");
        cb.selectedProperty().bindBidirectional(isChecked);

        // Some example application logic
        final Button stopButton = new Button("Fire stop");
        stopButton.setOnAction(event -> application.fireStop());

        final Button exitButton = new Button("Fire System.exit");
        exitButton.setOnAction(event -> System.exit(0));

        final Label titleLabel = new Label("Press the button or close the window to stop the Application.");
        final Label subtitle = new Label("Check the Checkbox to prevent the controller from stopping");
        final Label label = new Label("Test System.exit intercept with the other button.");

        final VBox labels = new VBox(5, titleLabel, subtitle, label);
        labels.setAlignment(Pos.CENTER);
        final HBox buttons = new HBox(5, stopButton, exitButton);
        buttons.setAlignment(Pos.CENTER);

        final VBox content = new VBox(10, labels, cb, buttons);
        content.setAlignment(Pos.CENTER);

        final BorderPane node = new BorderPane(content);
        node.setPadding(new Insets(20));
        return node;
    }

    @OnStart
    private void onStart() {
        application.setTitle("StartStop Controller example");
    }

    @OnStop
    private void onStop() {
        // Step 1: Check if this controller could stop
        if (isChecked.get())
            throw new StopInterruptedException();

        // Step 2: Tear down resources once all parent controllers run their checks and are capable of tearing down

        // Won't be able to use the field from now on
        isChecked = null;
    }

    // endregion Controller

}
