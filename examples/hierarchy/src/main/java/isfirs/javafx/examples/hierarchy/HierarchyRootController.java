package isfirs.javafx.examples.hierarchy;

import isfirs.javafx.annotations.ChildController;
import isfirs.javafx.annotations.OnStart;
import isfirs.javafx.controller.RootController;
import isfirs.javafx.examples.hierarchy.hierarchy.centerpanel.CenterPanelController;
import isfirs.javafx.examples.hierarchy.hierarchy.toppanel.TopPanelController;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import org.jetbrains.annotations.NotNull;

public final class HierarchyRootController extends RootController<BorderPane> {

    @ChildController
    private final TopPanelController topPanelController = new TopPanelController(this);
    @ChildController
    private final CenterPanelController centerPanelController = new CenterPanelController(this);

    private final ReadOnlyIntegerWrapper intProperty = new ReadOnlyIntegerWrapper(this, "intProperty", 0);

    private final HierarchyApplication application;

    /**
     * @param application A reference to the application
     */
    protected HierarchyRootController(final HierarchyApplication application) {
        super();
        this.application = application;
    }

    // region Controller

    @Override
    @NotNull
    protected BorderPane createNode() {

        final BorderPane node = new BorderPane();
        node.setPadding(new Insets(20));

        node.setTop(topPanelController.getNode());
        node.setCenter(centerPanelController.getNode());

        return node;
    }

    @OnStart
    private void onStart() {
        application.setTitle("Hierarchy Application example");
    }

    // endregion Controller

    // region HierarchyRootController

    public void increment() {
        intProperty.set(intProperty.get() + 1);
    }

    public ReadOnlyIntegerProperty getModel() {
        return intProperty.getReadOnlyProperty();
    }

    // endregion HierarchyRootController

}
