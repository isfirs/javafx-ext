package isfirs.javafx.examples.hierarchy;

interface HierarchyApplication {

    /**
     * Set the title on the primary stage.
     *
     * @param title The new stage title
     */
    void setTitle(String title);

}
