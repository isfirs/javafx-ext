/**
 * Expect all parameters are non null by default.
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.examples.hierarchy;

import javax.annotation.ParametersAreNonnullByDefault;