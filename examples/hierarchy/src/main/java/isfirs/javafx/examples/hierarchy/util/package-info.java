/**
 * Expect all parameters are non null by default.
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.examples.hierarchy.util;

import javax.annotation.ParametersAreNonnullByDefault;