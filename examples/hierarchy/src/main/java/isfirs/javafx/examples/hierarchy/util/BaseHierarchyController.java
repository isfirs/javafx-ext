package isfirs.javafx.examples.hierarchy.util;

import isfirs.javafx.examples.hierarchy.HierarchyRootController;
import javafx.scene.Node;

public abstract class BaseHierarchyController<N extends Node> extends isfirs.javafx.controller.Controller<N> {

    private final HierarchyRootController appRoot;

    protected BaseHierarchyController(HierarchyRootController appRoot) {
        super(appRoot);
        this.appRoot = appRoot;
    }

    public HierarchyRootController getAppRoot() {
        return appRoot;
    }

}
