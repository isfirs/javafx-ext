package isfirs.javafx.examples.hierarchy.hierarchy.toppanel;

import isfirs.javafx.examples.hierarchy.HierarchyRootController;
import isfirs.javafx.examples.hierarchy.util.BaseHierarchyController;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import org.jetbrains.annotations.NotNull;

public final class TopPanelController extends BaseHierarchyController<HBox> {

    public TopPanelController(final HierarchyRootController appRoot) {
        super(appRoot);
    }

    @NotNull
    @Override
    protected HBox createNode() {
        final Label titleLabel = new Label("Press the button to increase the counter");
        final HBox node = new HBox(titleLabel);
        node.setAlignment(Pos.CENTER);
        return node;
    }

}
