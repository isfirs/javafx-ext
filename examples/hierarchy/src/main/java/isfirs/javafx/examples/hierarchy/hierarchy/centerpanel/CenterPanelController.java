package isfirs.javafx.examples.hierarchy.hierarchy.centerpanel;

import isfirs.javafx.examples.hierarchy.HierarchyRootController;
import isfirs.javafx.examples.hierarchy.util.BaseHierarchyController;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

public final class CenterPanelController extends BaseHierarchyController<VBox> {

    public CenterPanelController(final HierarchyRootController appRoot) {
        super(appRoot);
    }

    @NotNull
    @Override
    protected VBox createNode() {
        final Button counterButton = new Button("Increase counter");
        counterButton.setOnAction(event -> getAppRoot().increment());
        final Label counterLabel = new Label();
        counterLabel.textProperty().bind(getAppRoot().getModel().asString("Counter: %d"));

        final VBox node = new VBox(10, counterLabel, counterButton);
        node.setAlignment(Pos.CENTER);
        return node;
    }

}
