
// The pattern we want to log with
def logPattern = '%d{yyyy-MM-dd HH:mm:SSS}\t%level\t[%thread]\t%logger.%M:%L\t%msg%n'

// Create a console appender
appender("console", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = logPattern
    }

}

// Set the root logger to use the console appender
root(DEBUG, ["console"])
