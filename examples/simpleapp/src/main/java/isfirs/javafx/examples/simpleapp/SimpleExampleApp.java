package isfirs.javafx.examples.simpleapp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public final class SimpleExampleApp extends Application implements SimpleApplication {

    private Stage primaryStage;

    public static void main(final String... args) {
        Application.launch(SimpleExampleApp.class, args);
    }

    @Override
    public void start(final Stage primaryStage) {
        this.primaryStage = primaryStage;

        // Step 1: Instantiate
        final SimpleAppRootController rootController = new SimpleAppRootController(this);

        // STep 2: Start up the controller
        rootController.start();

        //Step 3: Create scene and set it into stage
        final Scene scene = new Scene(rootController.getNode());
        primaryStage.setScene(scene);

        // Step 4: Show stage
        primaryStage.show();
    }

    @Override
    public void setTitle(final String title) {
        primaryStage.setTitle(title);
    }
}
