package isfirs.javafx.examples.simpleapp;

interface SimpleApplication {

    /**
     * Set the title on the primary stage.
     *
     * @param title The new stage title
     */
    void setTitle(String title);

}
