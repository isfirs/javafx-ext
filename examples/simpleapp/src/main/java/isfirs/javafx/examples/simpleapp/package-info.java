/**
 * Expect all parameters are non null by default.
 */
@ParametersAreNonnullByDefault
package isfirs.javafx.examples.simpleapp;

import javax.annotation.ParametersAreNonnullByDefault;