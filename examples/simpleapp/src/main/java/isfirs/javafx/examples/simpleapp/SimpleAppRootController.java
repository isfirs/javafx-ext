package isfirs.javafx.examples.simpleapp;

import isfirs.javafx.annotations.OnStart;
import isfirs.javafx.controller.RootController;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.jetbrains.annotations.NotNull;

final class SimpleAppRootController extends RootController<BorderPane> {

    private final SimpleApplication application;

    /**
     * @param application A reference to the application
     */
    protected SimpleAppRootController(final SimpleApplication application) {
        super();
        this.application = application;
    }

    @Override
    @NotNull
    protected BorderPane createNode() {
        // Some example application logic
        final IntegerProperty intProperty = new SimpleIntegerProperty(this, "intProperty", 0);

        final Button counterButton = new Button("Increase counter");
        counterButton.setOnAction(event -> intProperty.set(intProperty.get() + 1));
        final Label counterLabel = new Label();
        counterLabel.textProperty().bind(intProperty.asString("Counter: %d"));

        final Label titleLabel = new Label("Press the button to increase the counter");
        final VBox content = new VBox(10, titleLabel, counterLabel, counterButton);
        content.setAlignment(Pos.CENTER);

        final BorderPane node = new BorderPane(content);
        node.setPadding(new Insets(20));
        return node;
    }

    @OnStart
    private void onStart() {
        application.setTitle("Simple Application example");
    }
}
